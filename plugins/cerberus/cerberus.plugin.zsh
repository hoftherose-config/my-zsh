alias cerberus="ssh -X Cerberus -l cerberus"
alias cerberus_wake="wakeonlan -i 172.24.57.50 -p 9 0C:DD:24:3D:2B:0B"
alias cerberus_sleep="ssh Cerberus -l cerberus 'systemctl suspend'"

function cerberus_port() {
    ssh -nNT -L "$1":localhost:"$1" cerberus@Cerberus
}
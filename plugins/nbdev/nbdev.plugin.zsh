alias nbclean="nbdev_clean_nbs"
alias nblib="nbdev_build_lib"
alias nbdocs="nbdev_build_docs"

alias docs_serve="make docs_serve"

function nbprep () {
    nblib
    nbdocs
    nbclean
}
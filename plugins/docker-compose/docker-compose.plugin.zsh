# support Compose v2 as docker CLI plugin
# (( ${+commands[docker-compose]} )) && dccmd='docker-compose' || dccmd='docker compose'
dccmd='docker compose'

# alias dco="$dccmd"
# alias dcb="$dccmd build"
# alias dce="$dccmd exec"
# alias dcps="$dccmd ps"
# alias dcrestart="$dccmd restart"
# alias dcrm="$dccmd rm"
# alias dcr="$dccmd run"
alias dcstop="$dccmd stop"
# alias dcup="$dccmd up"
alias dcupb="$dccmd up --build"
alias up="$dccmd up -d"
alias dcup="$dccmd up -d"
alias down="$dccmd down"
alias dcdown="$dccmd down"
# alias dclog="$dccmd logs"
# alias dclf="$dccmd logs -f"
# alias dcpull="$dccmd pull"
# alias dcstart="$dccmd start"
# alias dck="$dccmd kill"
alias ld="lazydocker"
alias dra="docker rm \$(docker ps -a -f status=exited -q)"
alias drai="docker rmi \$(docker images -a -q)"

unset dccmd

#
# Functions
#

# The name of the current branch
# Back-compatibility wrapper for when this function was defined here in
# the plugin, before being pulled in to core lib/git.zsh as git_current_branch()
# to fix the core -> git plugin dependency.
function current_branch() {
  git_current_branch
}

# Pretty log messages
function _git_log_prettily(){
  if ! [ -z $1 ]; then
    git log --pretty=$1
  fi
}
compdef _git _git_log_prettily=git-log

# Warn if the current branch is a WIP
function work_in_progress() {
  if $(git log -n 1 2>/dev/null | grep -q -c "\-\-wip\-\-"); then
    echo "WIP!!"
  fi
}

#
# Aliases
# (sorted alphabetically)
#

alias g='git'
alias ghh='git help'
alias ginit='git init'
function gproject () {
    md $1
    ginit
}

alias ga='git add'
alias gaa='git add --all'
alias gs='git status'

alias gb='git branch'
alias gba='git branch -a'
alias gbd='git branch -d'
alias gbD='git branch -D'
alias gbr='git branch --remote'

alias gc='git commit -m'
alias gc!='git commit -m --amend'

alias gbc='git checkout -b'
alias gcl='git clone'

alias gd='git diff'

alias gf='git fetch'
alias gfo='git fetch origin'

alias ggpull='git pull origin "$(git_current_branch)"'
alias ggpush='git push origin "$(git_current_branch)"'

alias ggsup='git branch --set-upstream-to=origin/$(git_current_branch)'
alias gpsup='git push --set-upstream origin $(git_current_branch)'

alias gm='git merge'
alias gmom='git merge origin/master'
alias gmt='git mergetool --no-prompt'
alias gmtvim='git mergetool --no-prompt --tool=vimdiff'
alias gmum='git merge upstream/master'
alias gma='git merge --abort'


alias gpull='git pull'
alias gpush='git push'
alias gpush!='git push --force'
alias gpushu='git push -u origin main'

alias gr='git remote'
alias gra='git remote add'
alias grao='git remote add origin'
alias grb='git rebase'

alias gclean='git clean -f -d'
alias gcleani='git clean -di'
alias grs='git reset --soft'
alias grm='git reset --mixed'
alias grh='git reset --hard'

# use the default stash push on git 2.13 and newer
autoload -Uz is-at-least
is-at-least 2.13 "$(git --version 2>/dev/null | awk '{print $3}')" \
  && alias gsta='git stash push' \
  || alias gsta='git stash save'
